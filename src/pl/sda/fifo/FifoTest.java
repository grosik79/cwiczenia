package pl.sda.fifo;

public class FifoTest {
    public static void main(String[] args) {
        FIFOSet set = new FIFOSet();
        set.push("a");
        set.push("b");
        set.push("c");
        set.push("d");
        set.push("e");

        while(set.size() > 0){
            System.out.println(set.pop());
        }
    }
}
