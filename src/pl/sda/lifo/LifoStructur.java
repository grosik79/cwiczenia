package pl.sda.lifo;

import java.util.LinkedList;

public class LifoStructur<T> {

    private LinkedList<T> list = new LinkedList<T>();

    // umieszczanie
    public void push(T data) {
        list.add(data);//dodaje na koniec
    }

    //podglądanie następnego elementu
    public T peek() {
        if (list.size() == 0) {
            return null;
        }
        return list.get(0);

    }

    public T poll() {
        if (list.size() == 0) {
            return null;
        }
        T elementZwracany = list.get(0); // przypisuje do zmiennej
        list.remove(0);
        return elementZwracany;
    }

    public int size() {
        return list.size();
    }
}

