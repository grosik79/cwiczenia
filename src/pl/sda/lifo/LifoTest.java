package pl.sda.lifo;

public class LifoTest {
    public static void main(String[] args) {
        LifoStructur structur = new LifoStructur();
        structur.push(1);
        structur.push("abrbra");
        structur.push(" bla bla");
        structur.push(1);
        structur.push(3);
        structur.push("abra");

        while (structur.size() > 0){
            System.out.println(structur.poll());
        }
    }
}
